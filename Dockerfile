FROM node:10
RUN apt install python3
RUN git clone https://gitlab.com/Herobone/mai-ros-control-center.git
WORKDIR /mai-ros-control-center
RUN npm install
RUN npm run build
CMD cd dist && python3 -m http.server 8003