export class Setting {
  name: string;
  address: string;
  port: number;
  log: string;
  imagePreview: { port: number, quality: number, width: number, height: number };
  battery: boolean;
  batteryTopic: string;
  advanced: boolean;

  static getSettings(): Setting[] {
    const settings = [
      {
        'name': 'Global',
        'address': '192.168.1.110',
        'port': 8002,
        'log': '/rosout',
        'imagePreview': {
          'port': 8001,
          'quality': 70,
          'width': 640,
          'height': 480
        },
        'battery': false,
        'batteryTopic': '',
        'advanced': false
      }, {
        'name': 'Local Docker Subnet',
        'address': '172.17.0.3',
        'port': 8002,
        'log': '/rosout',
        'imagePreview': {
          'port': 0,
          'quality': 70,
          'width': 640,
          'height': 480
        },
        'battery': false,
        'batteryTopic': '',
        'advanced': false
      }, {
        'name': 'Local Normal Subnet',
        'address': '10.42.0.1',
        'port': 8002,
        'log': '/rosout',
        'imagePreview': {
          'port': 0,
          'quality': 70,
          'width': 640,
          'height': 480
        },
        'battery': false,
        'batteryTopic': '',
        'advanced': false
      }];

      return settings;
  }

  static getDefault(): Setting {
    return this.getSettings()[1];
  }

  static getCurrent(): Setting {
    const index = JSON.parse(localStorage.getItem('roscc2-index')) || 1;

    return this.getSettings()[index];
  }
}
